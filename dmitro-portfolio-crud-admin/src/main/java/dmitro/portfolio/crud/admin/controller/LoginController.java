package dmitro.portfolio.crud.admin.controller;

import dmitro.portfolio.crud.admin.LoginStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by dmitro on 24.01.2015.
 */

@Controller
@RequestMapping("/login")
public class LoginController {

    private static final Logger _logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    @Qualifier("authenticationManager")
    AuthenticationManager authenticationManager;

    /**
     * Spring Security default remember me service which provides direct access to the context from the remember me cookie.
     */
    @Autowired(required = true)
    private RememberMeServices rememberMeServices;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public LoginStatus getStatus(HttpServletRequest request, HttpServletResponse response) {

        _logger.debug("|******************************getStatus method invoked******************************|");

        Authentication authentication = getSessionAuthentication(request);

        if (isAuthenticated(authentication)) {

            _logger.debug("|********user is authenticated************|");
            return authenticationToLoginStatus(authentication);
        }

        Authentication rememberMeAuthentication = rememberMeServices.autoLogin(request, response);

        _logger.debug("************rememberMeAuthentication = " + rememberMeAuthentication);

        if (isAuthenticated(rememberMeAuthentication)) {

            _logger.debug("|********rememberMeAuthentication is used************|");
            return authenticationToLoginStatus(rememberMeAuthentication);
        }

        return authenticationToLoginStatus(authentication);
    }

    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    @ResponseBody
    public LoginStatus login(HttpServletRequest request, HttpServletResponse response, @RequestBody LoginStatus loginStatus) {

        _logger.debug("|******************************login method invoked******************************|");

        Authentication authentication = null;

        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(loginStatus.getUsername(), loginStatus.getPassword());
            authentication = authenticationManager.authenticate(token);
            setSessionAuthentication(request, authentication);
        } catch (BadCredentialsException e) {
            authentication = SecurityContextHolder.getContext().getAuthentication();
        }

        if (loginStatus.getRememberMe() != null && loginStatus.getRememberMe() && isAuthenticated(authentication)) {

            _logger.debug("|********rememberMeServices.loginSuccess method invoked************|");
            rememberMeServices.loginSuccess(request, response, authentication);
        }

        return authenticationToLoginStatus(authentication);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseBody
    public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        setSessionAuthentication(request, null);
        rememberMeServices.loginFail(request, response);
        response.getWriter().println("logged out");
    }

    /**
     * Get current session authentication.<br/>
     * As this controller is out of Spring Security filter,
     * the standard way to access Security Context (SecurityContextHolder.getContext()) is not
     * working and we have to do it the old way.
     *
     * @param request HttpServletRequest provided by Spring MVC to look for session attributes.
     * @return The current authentication in session or null if there is none.
     */
    private Authentication getSessionAuthentication(HttpServletRequest request) {
        SecurityContext securityContext = (SecurityContext) request.getSession().getAttribute(
                HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY);

        if (securityContext == null) {
            return null;
        } else {
            return securityContext.getAuthentication();
        }
    }

    private boolean isAuthenticated(Authentication authentication) {
        return authentication != null &&
                !authentication.getName().equals("anonymousUser") &&
                authentication.isAuthenticated();
    }

    private LoginStatus authenticationToLoginStatus(Authentication authentication) {
        LoginStatus loginStatus = new LoginStatus();

        if (isAuthenticated(authentication)) {
            loginStatus.setLoggedIn(true);
            loginStatus.setUsername(authentication.getName());
        } else {
            loginStatus.setLoggedIn(false);
            loginStatus.setUsername(null);
        }

        return loginStatus;
    }

    private void setSessionAuthentication(HttpServletRequest request, Authentication authentication) {
        SecurityContextHolder.getContext().setAuthentication(authentication);
        request.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());
    }
}
