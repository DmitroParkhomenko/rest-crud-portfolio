package dmitro.portfolio.crud.admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Created by dmitro on 03.02.2015.
 */

public class SessionListener implements HttpSessionListener {
    private static final Logger _logger = LoggerFactory.getLogger(SessionListener.class);

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        _logger.debug("==== Session is created ====");
        httpSessionEvent.getSession().setMaxInactiveInterval(3*60);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        _logger.debug("==== Session is destroyed ====");

    }
}
