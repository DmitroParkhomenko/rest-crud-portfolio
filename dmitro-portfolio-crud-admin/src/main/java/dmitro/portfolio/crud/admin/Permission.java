package dmitro.portfolio.crud.admin;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by dmitro on 28.01.2015.
 */

@Entity
public class Permission {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String permissionName;

    @ManyToMany(mappedBy = "permissions", fetch=FetchType.EAGER, cascade = {})
    private Set<Role> roles = new HashSet<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
