package dmitro.portfolio.crud.admin.authentication.dao;

import dmitro.portfolio.crud.admin.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by dmitro on 15.01.2015.
 */

@Service
public class UserServiceImpl implements UserDetailsService, InitializingBean {

    private static final Logger _logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired(required = true)
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByLogin(username);

        if (user == null) {
            _logger.debug("User is not found!");
            throw new UsernameNotFoundException(username + " n'existe pas");
        }

        String password = user.getPassword();

        boolean isEnabled = user.getStatus().equals(UserStatus.ACTIVE);
        boolean isCredentialsNonExpired = user.getStatus().equals(UserStatus.ACTIVE);
        boolean isAccountNonLocked = user.getStatus().equals(UserStatus.ACTIVE);
        boolean isAccountNonExpired = user.getStatus().equals(UserStatus.ACTIVE);

        Collection<GrantedAuthority> authorities = new ArrayList<>();

        for(Role role: user.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getRoleName()));

            for(Permission permission: role.getPermissions()) {
                authorities.add(new SimpleGrantedAuthority(permission.getPermissionName()));
            }
        }

        org.springframework.security.core.userdetails.User securityUser =
                new org.springframework.security.core.userdetails.User(username, password, isEnabled, isCredentialsNonExpired, isAccountNonExpired, isAccountNonLocked, authorities);

        _logger.debug("Created securityUser = " + securityUser.toString());

        return securityUser;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        _logger.debug("setting test uses data");

        User user = new User();
        user.setLogin("dmitro");
        user.setPassword("2424");
        user.setStatus(UserStatus.ACTIVE);
        Set<Role> roles = new HashSet<Role>();

        Role role = new Role();
        role.setRoleName("ROLE_ADMIN");
        Set<Permission> permissions = new HashSet<>();

        Permission permission = new Permission();
        permission.setPermissionName("PERM_SAVE_PRODUCT");
        permissions.add(permission);

        role.setPermissions(permissions);

        roles.add(role);
        user.setRoles(roles);
        userRepository.save(user);
    }
}