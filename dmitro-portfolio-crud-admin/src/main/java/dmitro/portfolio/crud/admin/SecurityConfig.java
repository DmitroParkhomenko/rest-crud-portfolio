package dmitro.portfolio.crud.admin;

import dmitro.portfolio.crud.admin.authentication.dao.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.RememberMeConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.rememberme.InMemoryTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Service;

/**
 * Created by dmitro on 27.12.2014.
 */

@Configuration
@EnableWebSecurity
@EnableJpaRepositories(basePackageClasses = {UserRepository.class, RoleRepository.class, ProductRepository.class})
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan(basePackages = "dmitro.portfolio.crud.admin.authentication.dao")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired(required = true)
    private UserDetailsService userDetailsService;

    @Autowired(required = true)
    private Http403ForbiddenEntryPoint http403ForbiddenEntryPoint;

    @Autowired(required = true)
    private PersistentTokenRepository persistentTokenRepository;

    @Autowired(required = true)
    private RememberMeServices rememberMeServices;

    @Autowired(required = true)
    private AuthenticationManager authenticationManager;

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
//        also it can be configured in a next way:
//        <intercept-url pattern="/login*" access="permitAll" />
        web
                .ignoring()
                .antMatchers(HttpMethod.GET, "/login")
                .antMatchers(HttpMethod.POST, "/login")
                .antMatchers(HttpMethod.PUT, "/login");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .httpBasic().authenticationEntryPoint(http403ForbiddenEntryPoint).
                and()
                .authorizeRequests()
                .antMatchers("/users/**", "/permissions/**", "/roles/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/login").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/products").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/products/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/products/**").hasRole("ADMIN").and()
                .csrf().disable();

        http.setSharedObject(AuthenticationManager.class, authenticationManager);
        RememberMeConfigurer<HttpSecurity> httpSecurityRememberMeConfigurer = http.rememberMe();
        httpSecurityRememberMeConfigurer.rememberMeServices(rememberMeServices);
        httpSecurityRememberMeConfigurer.key("uniqueAndSecret");
        httpSecurityRememberMeConfigurer.init(http);
        httpSecurityRememberMeConfigurer.configure(http);
    }

    @Bean(name = "authenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public Http403ForbiddenEntryPoint http403ForbiddenEntryPoint() {
        return new Http403ForbiddenEntryPoint();
    }

    @Bean
    public RememberMeServices persistentTokenBasedRememberMeServices() {
        return new PersistentTokenBasedRememberMeServices("uniqueAndSecret", userDetailsService, persistentTokenRepository);
    }

    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        return new InMemoryTokenRepositoryImpl();
    }
}
