package dmitro.portfolio.crud.admin;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.repository.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 * Created by dmitro on 27.12.2014.
 */

//@PreAuthorize("hasRole('ROLE_USER')")
@RestResource(path = "products", rel = "products")
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

    @PreAuthorize("hasRole('PERM_SAVE_PRODUCT')")
    @Override
    Product save(Product s);

    @PreAuthorize("hasRole('PERM_DEL_PRODUCT')")
    @Override
    void delete(Long aLong);
}
