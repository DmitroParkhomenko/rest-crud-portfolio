package dmitro.portfolio.crud.admin.utils;


import org.apache.log4j.RollingFileAppender;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by dmitro on 25.01.2015.
 */
public class NewLogForEachRunFileAppender extends RollingFileAppender {

    @Override
    public void setFile(String fileName)
    {
        if (fileName.indexOf("%timestamp") >= 0) {
            Date d = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            fileName = fileName.replaceAll("%timestamp", format.format(d));
        }
        super.setFile(fileName);
    }
}
