package dmitro.portfolio.crud.admin;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by dmitro on 17.01.2015.
 */

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = "dmitro.portfolio.crud.admin.controller")
public class WebApplicationConfiguration {
}
