package dmitro.portfolio.crud.admin.controller;

import dmitro.portfolio.crud.admin.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by dmitro on 05.02.2015.
 */

@Controller
@RequestMapping("/registration")
public class RegistrationController {
    private static final Logger _logger = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired(required = true)
    private UserRepository userRepository;

    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    @ResponseBody
    public LoginStatus registration(@RequestBody LoginStatus loginStatus) {
        _logger.debug("|******************************registration method invoked******************************|");

        LoginStatus newLogin = new LoginStatus();

        if(checkUserName(loginStatus.getUsername()) || checkPassword(loginStatus.getPassword())) {
            return newLogin;
        }

        User existedUser = userRepository.findByLogin(loginStatus.getUsername());

        if(existedUser == null) {
            return newLogin;
        }

        User user = new User();
        user.setLogin(loginStatus.getUsername());
        user.setPassword(loginStatus.getPassword());
        user.setStatus(UserStatus.ACTIVE);
        Set<Role> roles = new HashSet<Role>();

        Role role = new Role();
        role.setRoleName("ROLE_ADMIN");
        Set<Permission> permissions = new HashSet<>();

        Permission permission = new Permission();
        permission.setPermissionName("PERM_SAVE_PRODUCT");
        permissions.add(permission);

        role.setPermissions(permissions);

        roles.add(role);
        user.setRoles(roles);
        userRepository.save(user);

        newLogin.setUsername(loginStatus.getUsername());

        return newLogin;
    }

    private boolean checkPassword(String password) {
        //TODO here maybe more complex checking later
        return password.isEmpty();
    }

    private boolean checkUserName(String username) {
        //TODO here maybe more complex checking later
        return username.isEmpty();
    }
}
