package dmitro.portfolio.crud.admin;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.webmvc.RepositoryRestMvcConfiguration;


/**
 * Created by dmitro on 28.12.2014.
 */

@Configuration
public class CustRepositoryRestMvcConfiguration extends RepositoryRestMvcConfiguration {
}
