package dmitro.portfolio.crud.admin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.repository.annotation.RestResource;

/**
 * Created by dmitro on 23.01.2015.
 */

@RestResource(path = "roles", rel = "roles")
public interface RoleRepository extends JpaRepository<Role, Integer> {

}