package dmitro.portfolio.crud.admin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.repository.annotation.RestResource;


/**
 * Created by dmitro on 28.01.2015.
 */

@RestResource(path = "permissions", rel = "permissions")
public interface PermissionRepository extends JpaRepository<Permission, Integer> {
}
