package dmitro.portfolio.crud.admin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.repository.annotation.RestResource;

/**
 * Created by dmitro on 17.01.2015.
 */

@RestResource(path = "users", rel = "users")
public interface UserRepository extends JpaRepository<User, Integer> {

    /**
     * This query will be automatically implemented by it's name, "findBy" is the key work and "Login" is parsed as the criteria. By parsing the
     * parameters declared, the login match the "Login" from "findBy".
     *
     * @param login instance to be the value of the criteria
     * @return a single user matching the login
     */
    User findByLogin(String login);
}
