package dmitro.portfolio.crud.admin;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by dmitro on 30.12.2014.
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
