package dmitro.portfolio.crud.admin;

/**
 * Created by dmitro on 16.01.2015.
 */
public enum UserStatus {
    ACTIVE,
    INACTIVE
}
