package dmitro.portfolio.crud.admin.rest.integration;

import dmitro.portfolio.crud.admin.LoginStatus;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.*;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Created by dmitro on 05.02.2015.
 */

//Note that JUnit do NOT guaranty tests order!

public class LoginIntegrationTest {

    @Before
    public void setup() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    @Test
    public void checkGoodCredentionsForUserThatWasNotPassedAuthentication() {
        RestTemplate template = new RestTemplate();
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                getHeadersWithBasicAuthentication("dmitro" + ":" + "2424"));

        ResponseEntity<LoginStatus> entity = template.exchange(
                "http://localhost:8080/admin/login", HttpMethod.GET, requestEntity, LoginStatus.class);

        assertEquals(HttpStatus.OK, entity.getStatusCode());

        LoginStatus loginStatus = entity.getBody();

        assertTrue(!loginStatus.isLoggedIn());
        assertTrue(loginStatus.getUsername() == null);
        assertTrue(loginStatus.getPassword() == null);
        assertTrue(loginStatus.getRememberMe() == null);
    }

    @Test
    public void logInWithTestValidUser() {
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                "{\"username\": \"dmitro\", \"password\": \"2424\", \"rememberMe\": \"true\"}",
                getHeadersForJsonType());
        RestTemplate template = new RestTemplate();
        ResponseEntity<LoginStatus> entity = template.postForEntity(
                "http://localhost:8080/admin/login?_spring_security_remember_me={remember_me_value}",
                requestEntity, LoginStatus.class, "true");

        assertEquals(HttpStatus.OK, entity.getStatusCode());

        LoginStatus loginStatus = entity.getBody();

        assertTrue(loginStatus.isLoggedIn());
        assertTrue(loginStatus.getUsername().equals("dmitro"));
        assertTrue(loginStatus.getPassword() == null);
        assertTrue(loginStatus.getRememberMe() == null);

        deleteWithGoodCredentials();
    }

    @Test
    public void deleteWithBadCredentials() {
        RestTemplate template = new RestTemplate();
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                getHeadersWithBasicAuthentication("BAD_PRINCIPAL" + ":" + "BAD_CREDENTIAL"));

        try {
            template.exchange("http://localhost:8080/admin/login", HttpMethod.DELETE, requestEntity, String.class);
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.FORBIDDEN, ex.getStatusCode());
            return;
        }

        fail("Logout is with bad user not worked!");
    }

    @Test
    public void checkThatLoginStatusCanBeFetchedFromAnonumousUser() {
        RestTemplate template = new RestTemplate();
        ResponseEntity<LoginStatus> entity = template.getForEntity(
                "http://localhost:8080/admin/login",
                LoginStatus.class);

        assertEquals(HttpStatus.OK, entity.getStatusCode());

        LoginStatus loginStatus = entity.getBody();

        assertTrue(!loginStatus.isLoggedIn());
        assertTrue(loginStatus.getUsername() == null);
        assertTrue(loginStatus.getPassword() == null);
        assertTrue(loginStatus.getRememberMe() == null);
    }

    private void deleteWithGoodCredentials() {
        RestTemplate template = new RestTemplate();
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                getHeadersWithBasicAuthentication("dmitro" + ":" + "2424"));

        try {
            template.exchange("http://localhost:8080/admin/login", HttpMethod.DELETE, requestEntity, String.class);
        } catch (HttpClientErrorException ex) {
            fail("Logout is with good user not worked!");
            return;
        }

        assertTrue("User is logout!", true);
    }

    static HttpHeaders getHeadersWithBasicAuthentication(String auth) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        byte[] encodedAuthorisation = Base64.encode(auth.getBytes());
        headers.add("Authorization", "Basic "
                + new String(encodedAuthorisation));

        return headers;
    }

    static HttpHeaders getHeadersForJsonType() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }
}
