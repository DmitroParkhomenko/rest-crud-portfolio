define([
    'jquery',
    'underscore',
    'backbone',
    'models/model',
    // Using the Require.js text! plugin, we are loaded raw text
    // which will be used as our views primary template
    'text!templates/list.html'
], function($, _, Backbone, Model, tmpl){
    var ListView = Backbone.View.extend({
        el: $('#listBlock'),
        template: _.template(tmpl),

        initialize: function(){
            this.model = new Model();
        },

        render: function() {
            console.log('Model before render = ', JSON.stringify(this.model.attributes));
            this.$el.html(this.template({ data: this.model.attributes }));
        },

        fetch: function(callback) {
            console.log("run fetch");

            this.model.fetch({
                success: function() {
                    callback();
                },
                error: function() {
                    alert("Error!");
                }
            });
        }
    });
    // Our module now returns our view
    return ListView;
});
