define([
    'underscore',
    'backbone'
], function(_, Backbone){
    var Model = Backbone.Model.extend({
        urlRoot:'http://localhost:8080/translation-jpa/translation'
    });

    // Return the model for the module
    return Model;
});
