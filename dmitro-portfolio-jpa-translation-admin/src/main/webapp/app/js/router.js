define([
    'jquery',
    'underscore',
    'backbone',
    'views/view'
], function($, _, Backbone, TranslationView){
    var AppRouter = Backbone.Router.extend({
        routes: {
            // Define some URL routes
            //'translations': 'translations',

            // Default1
            '*actions': 'defaultAction'
        }
    });

    var initialize = function(){
        var app_router = new AppRouter;

        var listView = new TranslationView();

        listView.fetch(function() {
            listView.render();
        });

        //app_router.on('translations', function(){
        //    // Call render on the module we loaded in via the dependency array
        //    // 'views/projects/list'
        //    var listView = new TranslationView();
        //
        //    listView.fetch(function() {
        //        listView.render();
        //    });
        //});
        //
        app_router.on('defaultAction', function(actions){
            // We have no matching route, lets just log what the URL was
            console.log('No route:', actions);
        });

        Backbone.history.start();
    };

    return {
        initialize: initialize
    };
});
