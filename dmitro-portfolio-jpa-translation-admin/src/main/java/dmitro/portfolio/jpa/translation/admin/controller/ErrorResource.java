package dmitro.portfolio.jpa.translation.admin.controller;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by dmitro on 14.02.2015.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResource {
    private String code;
    private String message;

    public ErrorResource() { }

    public ErrorResource(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() { return code; }

    public void setCode(String code) { this.code = code; }

    public String getMessage() { return message; }

    public void setMessage(String message) { this.message = message; }
}
