package dmitro.portfolio.jpa.translation.admin.controller;

import dmitro.portfolio.jpa.translation.admin.model.*;
import dmitro.portfolio.jpa.translation.admin.model.ui.ScreenUi;
import dmitro.portfolio.jpa.translation.admin.model.ui.ScreenUi2;
import dmitro.portfolio.jpa.translation.admin.model.ui.WordUi;
import dmitro.portfolio.jpa.translation.admin.service.TranslationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * Created by dmitro on 23.11.2014.
 */
@Controller
@RequestMapping("/translation")
@SessionAttributes("oddDataLoaded")
public class TranslationController {

    private static final Logger _logger = LoggerFactory.getLogger(TranslationController.class);

    @Autowired
    private TranslationService translationService;

//Be aware about SessionAttributes: it cleans only when sessionStatus.setComplite() method called!!!!!!!!
//Profit from: http://stackoverflow.com/questions/26387233/spring-mvc-the-sessionattributes-and-status-setcomplete/26387605#26387605

    @ModelAttribute("oddDataLoaded")
    public void populateModel(Model model) {
        Map<String, Object> result = new HashMap<String, Object>(2);
        result.put("screenList", this.translationService.getAllScreens());
        result.put("countryList", this.translationService.getAllCountries());
        model.addAttribute("oddDataLoaded", result);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> listTranslation(
            @ModelAttribute("oddDataLoaded") Map<String, Object> oddDataLoaded, HttpSession session) {
        List<Screen> screenList = (List<Screen>) oddDataLoaded.get("screenList");
        List<Translation> translationList = this.translationService.listTranslation();
        Set<ScreenUi2> preparedData = DataUtil.prepareData(translationList, screenList);
        oddDataLoaded.put("translationList", preparedData);
        return oddDataLoaded;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{screenId}")
    @ResponseBody
    public Map<String, Object> listTranslation(@PathVariable String screenId) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("list", this.translationService.listTranslationByCriteria(screenId));
        return result;
    }

    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    public ResponseEntity<Translation> createTranslation(@RequestBody Translation translation) {
        this.translationService.addTranslation(translation);
        return new ResponseEntity<Translation>(HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Translation> deleteTranslation(@PathVariable String id) {

        int parsedId;

        try {
            parsedId = Integer.parseInt(id);
        } catch (NumberFormatException ex) {
            return new ResponseEntity<Translation>(HttpStatus.NOT_FOUND);
        }

        Translation translation = this.translationService.removeTranslation(parsedId);

        if (translation == null) {
            return new ResponseEntity<Translation>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Translation>(translation, HttpStatus.FORBIDDEN);
    }
}