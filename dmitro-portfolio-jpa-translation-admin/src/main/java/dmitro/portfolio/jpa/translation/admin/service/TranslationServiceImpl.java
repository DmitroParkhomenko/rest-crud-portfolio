package dmitro.portfolio.jpa.translation.admin.service;

import dmitro.portfolio.jpa.translation.admin.dao.TranslationDAO;
import dmitro.portfolio.jpa.translation.admin.model.Country;
import dmitro.portfolio.jpa.translation.admin.model.Screen;
import dmitro.portfolio.jpa.translation.admin.model.Translation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by dmitro on 23.11.2014.
 */
@Service
public class TranslationServiceImpl implements TranslationService {

    private TranslationDAO translationDAO;

    public void setTranslationDAO(TranslationDAO personDAO) {
        this.translationDAO = personDAO;
    }

    @Override
    @Transactional
    public List<Screen> getAllScreens() {
        return this.translationDAO.getAllScreens();
    }

    @Override
    @Transactional
    public List<Country> getAllCountries() {
        return this.translationDAO.getAllCountries();
    }

    @Override
    @Transactional
    public void addTranslation(Translation p) {
        this.translationDAO.addTranslation(p);
    }

    @Override
    @Transactional
    public Translation updateTranslation(Translation p) {
        return this.translationDAO.updateTranslation(p);
    }

    @Override
    @Transactional
    public List<Translation> listTranslation() {
        return this.translationDAO.listTranslation();
    }

    @Override
    public List<Translation> listTranslationByCriteria(String criteria) {
        return this.translationDAO.listTranslationByCriteria(criteria);
    }

    @Override
    @Transactional
    public Translation getTranslationById(int id) {
        return this.translationDAO.getTranslationById(id);
    }

    @Override
    @Transactional
    public Translation removeTranslation(int id) {
        return this.translationDAO.removeTranslation(id);
    }
}
