package dmitro.portfolio.jpa.translation.admin.model.ui;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dmitro on 17.02.2015.
 */
public class ScreenUi {
    private  int id;
    private Map<String, WordUi> words = new HashMap<String, WordUi>();

    public Map<String, WordUi> getWords() {
        return words;
    }

    public void setWords(Map<String, WordUi> words) {
        this.words = words;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ScreenUi screenUi = (ScreenUi) o;

        if (id != screenUi.id) return false;
        if (!words.equals(screenUi.words)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
