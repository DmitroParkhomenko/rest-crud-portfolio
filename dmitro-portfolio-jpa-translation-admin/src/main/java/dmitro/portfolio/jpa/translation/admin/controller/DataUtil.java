package dmitro.portfolio.jpa.translation.admin.controller;

import dmitro.portfolio.jpa.translation.admin.model.Screen;
import dmitro.portfolio.jpa.translation.admin.model.Translation;
import dmitro.portfolio.jpa.translation.admin.model.ui.ScreenUi;
import dmitro.portfolio.jpa.translation.admin.model.ui.ScreenUi2;
import dmitro.portfolio.jpa.translation.admin.model.ui.WordUi;

import java.util.*;

/**
 * Created by dmitro on 18.02.2015.
 */
public class DataUtil {
    public static Set<ScreenUi2> prepareData(List<Translation> translations, List<Screen> screens) {
        Map<Integer, ScreenUi> result = new HashMap<Integer, ScreenUi>(screens.size());

        for (Screen item : screens) {
            ScreenUi screenUi = new ScreenUi();
            screenUi.setId(item.getId());
            result.put(item.getId(), screenUi);
        }

        for (Translation item : translations) {
            int id = item.getScreen().getId();
            ScreenUi screenUi = result.get(id);
            Map<String, WordUi> words = screenUi.getWords();
            String wordId = item.getWordId();
            WordUi wordUi = words.get(wordId);

            if (wordUi == null) {
                wordUi = new WordUi();
                wordUi.setId(item.getId());
                wordUi.setName(wordId);
            }

            String translation = item.getTranslation();
            wordUi.getTranslations().put(item.getCountry().getId(), translation);
            words.put(wordId, wordUi);
            result.put(id, screenUi);
        }

        return getScreenUI2Set(result);
    }

    private static Set<ScreenUi2> getScreenUI2Set(Map<Integer, ScreenUi> map) {
        Set<ScreenUi2> result = new HashSet<ScreenUi2>();

        for (ScreenUi screenUi : map.values()) {
            Set<WordUi> words = new HashSet<WordUi>(screenUi.getWords().values());
            ScreenUi2 screenUi2 = new ScreenUi2(screenUi.getId(), words);
            result.add(screenUi2);
        }

        return result;
    }
}
