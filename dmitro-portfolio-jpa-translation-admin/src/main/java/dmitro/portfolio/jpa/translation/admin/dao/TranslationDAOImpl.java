package dmitro.portfolio.jpa.translation.admin.dao;

import dmitro.portfolio.jpa.translation.admin.model.Country;
import dmitro.portfolio.jpa.translation.admin.model.Screen;
import dmitro.portfolio.jpa.translation.admin.model.Translation;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by dmitro on 21.11.2014.
 */

@Repository
public class TranslationDAOImpl implements TranslationDAO {
    private static final Logger _logger = LoggerFactory.getLogger(TranslationDAOImpl.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

    @Override
    public List<Screen> getAllScreens() {
        return sessionFactory.getCurrentSession().createCriteria(Screen.class).list();
    }

    @Override
    public List<Country> getAllCountries() {
        return sessionFactory.getCurrentSession().createCriteria(Country.class).list();
    }

    @Override
    public void addTranslation(Translation obj) {
        sessionFactory.getCurrentSession().persist(obj);
    }

    @Override
    public Translation updateTranslation(Translation p) {
        return null;
    }

    @Override
    public List<Translation> listTranslation() {
        return sessionFactory.getCurrentSession().createCriteria(Translation.class).list();
    }

    @Override
    public List<Translation> listTranslationByCriteria(String criteria) {
        return null;
    }

    @Override
    public Translation getTranslationById(int id) {
        return null;
    }

    @Override
    public Translation removeTranslation(int id) {
        return null;
    }
}
