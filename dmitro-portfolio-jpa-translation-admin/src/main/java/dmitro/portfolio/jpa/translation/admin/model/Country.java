package dmitro.portfolio.jpa.translation.admin.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by dmitro on 08.02.2015.
 */
@Entity
public class Country implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "countryId")
    private int id;
    private String language;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "Country{" +
                "id=" + id +
                ", language='" + language + '\'' +
                '}';
    }
}
