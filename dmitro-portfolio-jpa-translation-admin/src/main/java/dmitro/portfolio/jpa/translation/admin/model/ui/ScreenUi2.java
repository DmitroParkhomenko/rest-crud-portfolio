package dmitro.portfolio.jpa.translation.admin.model.ui;

import java.util.Set;

/**
 * Created by dmitro on 18.02.2015.
 */
public class ScreenUi2 {
    private  int id;
    private Set<WordUi> words;

    public ScreenUi2(int id, Set<WordUi> words) {
        this.id = id;
        this.words = words;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ScreenUi2 screenUi2 = (ScreenUi2) o;

        if (id != screenUi2.id) return false;
        if (!words.equals(screenUi2.words)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + words.hashCode();
        return result;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<WordUi> getWords() {
        return words;
    }

    public void setWords(Set<WordUi> words) {
        this.words = words;
    }
}
