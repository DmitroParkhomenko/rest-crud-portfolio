package dmitro.portfolio.jpa.translation.admin.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by dmitro on 08.02.2015.
 */
@Entity
public class Screen implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "screenId")
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Screen{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
