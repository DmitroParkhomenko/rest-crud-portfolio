package dmitro.portfolio.jpa.translation.admin.model.ui;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dmitro on 17.02.2015.
 */
public class WordUi {
    private int id;
    private String name;
    private Map<Integer, String> translations = new HashMap<Integer, String>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Integer, String> getTranslations() {
        return translations;
    }

    public void setTranslations(Map<Integer, String> translations) {
        this.translations = translations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WordUi wordUi = (WordUi) o;

        if (id != wordUi.id) return false;
        if (!name.equals(wordUi.name)) return false;
        if (!translations.equals(wordUi.translations)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        return result;
    }
}
