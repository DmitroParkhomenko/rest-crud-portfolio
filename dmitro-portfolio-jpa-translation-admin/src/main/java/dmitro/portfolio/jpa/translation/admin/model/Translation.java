package dmitro.portfolio.jpa.translation.admin.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * Created by dmitro on 21.11.2014.
 */
@Entity
@Table(name = "Translation")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Translation {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
    private String wordId;

    @ManyToOne(optional=false)
    @JoinColumn(name = "country_countryId")
    private Country country;

    @ManyToOne(optional=false)
    @JoinColumn(name = "screen_screenId")
    private Screen screen;

    private String translation;

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWordId() {
        return wordId;
    }

    public void setWordId(String wordId) {
        this.wordId = wordId;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Screen getScreen() {
        return screen;
    }

    public void setScreen(Screen screen) {
        this.screen = screen;
    }

    @Override
    public String toString() {
        return "Translation{" +
                "id=" + id +
                ", wordId='" + wordId + '\'' +
                ", country=" + (country != null ? country : "") +
                ", screen=" + (screen != null ? screen : "") +
                ", translation='" + translation + '\'' +
                '}';
    }
}
