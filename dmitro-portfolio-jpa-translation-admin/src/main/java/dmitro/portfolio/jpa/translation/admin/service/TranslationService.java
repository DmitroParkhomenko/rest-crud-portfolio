package dmitro.portfolio.jpa.translation.admin.service;

import dmitro.portfolio.jpa.translation.admin.model.Country;
import dmitro.portfolio.jpa.translation.admin.model.Screen;
import dmitro.portfolio.jpa.translation.admin.model.Translation;

import java.util.List;

/**
 * Created by dmitro on 23.11.2014.
 */

public interface TranslationService {
    List<Screen> getAllScreens();
    List<Country> getAllCountries();
    void addTranslation(Translation p);
    Translation updateTranslation(Translation p);
    List<Translation> listTranslation();
    List<Translation> listTranslationByCriteria(String criteria);
    Translation getTranslationById(int id);
    Translation removeTranslation(int id);
}